package model.vo;

import model.data_structures.BinaryHeapArray;
import model.data_structures.LinearProbingHashST;

public class Taxi implements Comparable<Taxi> {

    private String taxi_id;
    private String company;

    public LinearProbingHashST<Integer, BinaryHeapArray<Servicio>> getMapaA1() {
        return mapaA1;
    }

    public void setMapaA1(LinearProbingHashST<Integer, BinaryHeapArray<Servicio>> mapaA1) {
        this.mapaA1 = mapaA1;
    }

    private LinearProbingHashST<Integer,BinaryHeapArray<Servicio>> mapaA1;
    public Taxi(String pTaxi, String pCompany) {
        taxi_id = pTaxi;
        company = pCompany;


    }



    @Override
    public int compareTo(Taxi o)
    {
        // TODO Auto-generated method stub
        return this.getTaxi_id().compareTo(o.getTaxi_id());
    }

    public String getTaxi_id() {
        return taxi_id;
    }

    public void setTaxi_id(String taxi_id) {
        this.taxi_id = taxi_id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

}
