package model.vo;


public class Compania implements Comparable<Compania> {
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    private String nombre;
    public Compania(String pNombre){
        nombre = pNombre;
    }

    @Override
    public int compareTo(Compania o) {
        return this.nombre.compareToIgnoreCase(o.nombre);
    }
}
