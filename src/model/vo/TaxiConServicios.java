package model.vo;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Lista;

public class TaxiConServicios implements Comparable<TaxiConServicios>{

    private String taxiId;
    private String compania;
    private IList<Servicio> servicios;

    public TaxiConServicios(String taxiId, String compania){
        this.taxiId = taxiId;
        this.compania = compania;
        this.servicios = new DoubleLinkedList<>(); // inicializar la lista de servicios
    }

    public String getTaxiId() {
        return taxiId;
    }

    public String getCompania() {
        return compania;
    }

    public IList<Servicio> getServicios()
    {
    	return servicios;
    }
    
    public int numeroServicios(){
        return servicios.size();
    }

    public void agregarServicio(Servicio servicio){
        servicios.add(servicio);
    }

    @Override
    public int compareTo(TaxiConServicios o) {
        return taxiId.compareTo( o.getTaxiId());
    }

    public void print(){
        System.out.println(numeroServicios()+" servicios "+" Taxi: "+taxiId);
        int i=0;
        while(i<servicios.size()){
            Servicio s = servicios.get(i);
            System.out.println("\t"+s.getTrip_start_timestamp());
            i++;
        }
        System.out.println("___________________________________");;
    }
}
