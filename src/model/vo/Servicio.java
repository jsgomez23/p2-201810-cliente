package model.vo;

import java.util.Date;

public class Servicio implements Comparable<Servicio>{
    private String trip_id;
    private String taxi_id;
    private int trip_seconds;
    private int pickup_community_area;
    private Date fechaHoraInico;
    private String trip_start_timestamp;
    private double trip_miles;
    private int dropoff_community_area;
    private double trip_total;
    private double pickup_centroid_latitude;
    private double pickup_centroid_longitude;
    private double relativePosition;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    private String company;
    public double getRelativePosition(){return relativePosition;}

    public void setRelativePosition(Double relativePosition){this.relativePosition = relativePosition;}

    public Servicio(String company, String trip_id, String taxi_id, int trip_seconds, int pickup_community_area, Date fechaHoraInico, String trip_start_timestamp, double trip_miles, int dropoff_community_area, double trip_total, double pickup_centroid_latitude, double pickup_centroid_longitude) {
        this.relativePosition = 0;
        this.company = company;
        this.trip_id = trip_id;
        this.taxi_id = taxi_id;
        this.trip_seconds = trip_seconds;
        this.pickup_community_area = pickup_community_area;
        this.fechaHoraInico = fechaHoraInico;
        this.trip_start_timestamp = trip_start_timestamp;
        this.trip_miles = trip_miles;
        this.dropoff_community_area = dropoff_community_area;
        this.trip_total = trip_total;
        this.pickup_centroid_latitude = pickup_centroid_latitude;
        this.pickup_centroid_longitude = pickup_centroid_longitude;
    }


    @Override
        public int compareTo(Servicio arg0) {
            // TODO Auto-generated method stub
            return this.getFechaHoraInico().compareTo( arg0.getFechaHoraInico() );
        }

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getTaxi_id() {
        return taxi_id;
    }

    public void setTaxi_id(String taxi_id) {
        this.taxi_id = taxi_id;
    }

    public int getTrip_seconds() {
        return trip_seconds;
    }

    public void setTrip_seconds(int trip_seconds) {
        this.trip_seconds = trip_seconds;
    }

    public int getPickup_community_area() {
        return pickup_community_area;
    }

    public void setPickup_community_area(int pickup_community_area) {
        this.pickup_community_area = pickup_community_area;
    }

    public Date getFechaHoraInico() {
        return fechaHoraInico;
    }

    public void setFechaHoraInico(Date fechaHoraInico) {
        this.fechaHoraInico = fechaHoraInico;
    }

    public String getTrip_start_timestamp() {
        return trip_start_timestamp;
    }

    public void setTrip_start_timestamp(String trip_start_timestamp) {
        this.trip_start_timestamp = trip_start_timestamp;
    }

    public double getTrip_miles() {
        return trip_miles;
    }

    public void setTrip_miles(double trip_miles) {
        this.trip_miles = trip_miles;
    }

    public int getDropoff_community_area() {
        return dropoff_community_area;
    }

    public void setDropoff_community_area(int dropoff_community_area) {
        this.dropoff_community_area = dropoff_community_area;
    }

    public double getTrip_total() {
        return trip_total;
    }

    public void setTrip_total(double trip_total) {
        this.trip_total = trip_total;
    }

    public double getPickup_centroid_latitude() {
        return pickup_centroid_latitude;
    }

    public void setPickup_centroid_latitude(double pickup_centroid_latitude) {
        this.pickup_centroid_latitude = pickup_centroid_latitude;
    }

    public double getPickup_centroid_longitude() {
        return pickup_centroid_longitude;
    }

    public void setPickup_centroid_longitude(double pickup_centroid_longitude) {
        this.pickup_centroid_longitude = pickup_centroid_longitude;
    }
}