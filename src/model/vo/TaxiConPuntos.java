package model.vo;

public class TaxiConPuntos implements Comparable<TaxiConPuntos> {


	private String taxi_id;
	private String company;
	private double puntos;
	private double dinero;
	private int numeroServicios;
	private double millas;

	public TaxiConPuntos(String taxi_id, String company, double dinero, double millas) {
		this.taxi_id = taxi_id;
		this.company = company;

		this.dinero = dinero;
		this.numeroServicios = 1;
		this.millas = millas;
		this.puntos = (dinero / millas) * numeroServicios;
	}

	public String getTaxi_id() {
		return taxi_id;
	}

	public void setTaxi_id(String taxi_id) {
		this.taxi_id = taxi_id;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public int getNumeroServicios() {
		return numeroServicios;
	}

	public void setNumeroServicios(int numeroServicios) {
		this.numeroServicios = numeroServicios;
	}


	public double getPuntos() {

		return puntos;
	}

	public void setPuntos(double puntos) {
		this.puntos = puntos;
	}

	public double getDinero() {
		return dinero;
	}

	public void setDinero(double dinero) {
		this.dinero = dinero;
	}

	public double getMillas() {
		return millas;
	}

	public void setMillas(double millas) {
		this.millas = millas;
	}

	public int compareTo(TaxiConPuntos o) {
		if (puntos > o.getPuntos()) {
			return 1;
		} else if (puntos < o.getPuntos()) {
			return -1;
		} else {
			return 0;
		}
	}
}
