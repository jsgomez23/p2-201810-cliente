package model.logic;

import API.ITaxiTripsManager;
import model.data_structures.*;
import model.vo.*;
import com.google.gson.Gson;

import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Stack;

public class TaxiTripsManager implements ITaxiTripsManager {
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	public SeparateChainingHashST <Integer, RedBlackBST<String, DoubleLinkedList<Servicio>>> coordenates = new SeparateChainingHashST<>();
	public Stack<Servicio> temp;
	public double[] averageCoordinates;
	private double[] counter;
	@Override
	public boolean cargarSistema(String direccionJson) {
		// TODO Auto-generated method stub
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			DecimalFormat df = new DecimalFormat("#.0");
			Gson gson = new Gson();
			ArrayList<Servicio> listaServ = new ArrayList<>();
			Servicio[] arregloServicios =gson.fromJson(new FileReader(direccionJson), Servicio[].class);
			coordenates=coordenates==null? new SeparateChainingHashST<>(temp.size()):coordenates;
			for(int i = 0; i<arregloServicios.length;i++) {
				Servicio act = arregloServicios[i];
				String fecha = act.getTrip_start_timestamp();
				Date x = sdf.parse(fecha);
				act.setFechaHoraInico(x);
				String millasUnDecimal = df.format(act.getTrip_miles());
				double millas = Double.parseDouble(millasUnDecimal);
				act.setTrip_miles(millas);
				if (act.getCompany() == null) {
					act.setCompany("Independent Owner");
				}
				String comp = act.getCompany();
				Taxi nuevo = new Taxi(act.getTaxi_id(), act.getCompany());
				if (i == 1) {
					System.out.println("Objeto taxi : id: " + nuevo.getTaxi_id() + " compania: " + nuevo.getCompany());
					System.out.println("millas recorridas: " + act.getTrip_miles());
				}
				if (companias.contains(comp)) {
					int posTaxi = -1;
					for (int j = 0; j < companias.get(comp).size(); j++) {
						Taxi actual = companias.get(comp).get(j);
						//System.out.println("entramos al for");
						if (actual.getTaxi_id().equalsIgnoreCase(nuevo.getTaxi_id())) {
							//System.out.println("cambio la posicion de -1 a "+j);
							posTaxi = j;
							break;
						}
					}
					if (posTaxi != -1) {
						if (companias.get(comp).get(posTaxi).getMapaA1().get(act.getPickup_community_area()) != null) {
							//ystem.out.println("Aqui no debe entrar");
							companias.get(comp).get(posTaxi).getMapaA1().get(act.getPickup_community_area()).add(act);
						} else {
							//System.out.println("Aqui debe entrar");
							BinaryHeapArray<Servicio> bh = new BinaryHeapArray<>(0);
							bh.add(act);
							companias.get(comp).get(posTaxi).getMapaA1().put(act.getPickup_community_area(), bh);

						}
					} else {
						BinaryHeapArray<Servicio> serv = new BinaryHeapArray<>(0);
						serv.add(act);
						LinearProbingHashST<Integer, BinaryHeapArray<Servicio>> mapaA1 = new LinearProbingHashST<>();
						mapaA1.put(act.getPickup_community_area(), serv);
						nuevo.setMapaA1(mapaA1);
						companias.get(comp).add(nuevo);
					}
				} else {
					BinaryHeapArray<Servicio> serv = new BinaryHeapArray<>(null);
					serv.add(act);
					LinearProbingHashST<Integer, BinaryHeapArray<Servicio>> mapaA1 = new LinearProbingHashST<>();
					mapaA1.put(act.getPickup_community_area(), serv);
					nuevo.setMapaA1(mapaA1);
					BinaryHeapArray<Taxi> taxisOrdenadosPorId = new BinaryHeapArray<>(null);
					taxisOrdenadosPorId.add(nuevo);
					companias.put(comp, taxisOrdenadosPorId);
				}
				int mod60 = act.getTrip_seconds()%60;
				int grupo = mod60==0? act.getTrip_seconds()/60:(act.getTrip_seconds()/60)+1;
				if(hashA2.contains(grupo)){
					hashA2.get(grupo).add(act);
				}
				else{
					ArrayList<Servicio> nuevoA2 = new ArrayList<>();
					nuevoA2.add(act);
					hashA2.put(grupo,nuevoA2);
				}


				if (arbolB1.isEmpty()) {
					ArrayList<Servicio> serviciosAsociados = new ArrayList<>();
					serviciosAsociados.add(act);
					arbolB1.put(act.getTrip_miles(), serviciosAsociados);
				} else {
					if (arbolB1.contains(act.getTrip_miles())) {
						arbolB1.get(act.getTrip_miles()).add(act);
					} else {
						ArrayList<Servicio> servNuevo = new ArrayList<>();
						servNuevo.add(act);
						arbolB1.put(act.getTrip_miles(), servNuevo);
					}
				}
				String keyB2 = act.getPickup_community_area() + "-" + act.getDropoff_community_area();
				Date keyTree = act.getFechaHoraInico();
				if (mapaB2.isEmpty()) {
					RedBlackBST<Date, ArrayList> fechas = new RedBlackBST<>();
					listaServ.add(act);
					fechas.put(act.getFechaHoraInico(), listaServ);
					mapaB2.put(keyB2, fechas);
				} else {
					if (mapaB2.contains(keyB2)) {
						listaServ.add(act);
						mapaB2.get(keyB2).put(act.getFechaHoraInico(), listaServ);
					} else {
						RedBlackBST<Date, ArrayList> fechas = new RedBlackBST<>();
						listaServ.add(act);
						fechas.put(act.getFechaHoraInico(), listaServ);
						mapaB2.put(keyB2, fechas);
					}
				}
				String keyC1 = act.getTaxi_id();
				Double miles = act.getTrip_miles();
				Double money = act.getTrip_total();
				TaxiConPuntos a = new TaxiConPuntos(act.getTaxi_id(), act.getCompany(), act.getTrip_total(), act.getTrip_miles());
				for (int k = 0; k < heapC1.size(); k++) {
					TaxiConPuntos actual = heapC1.get(k);
					if (actual.getTaxi_id().equals(a.getTaxi_id())) {
						actual.setDinero(actual.getDinero() + a.getDinero());
						actual.setMillas(actual.getMillas() + a.getMillas());
						actual.setNumeroServicios(actual.getNumeroServicios() + 1);
						actual.setPuntos(actual.getPuntos() + a.getPuntos());
					} else {
						heapC1.add(a);
					}
					if (act.getPickup_centroid_latitude()!=0)
					{
						this.temp.push(act);
						averageCoordinates[0] += act.getPickup_centroid_latitude();
						averageCoordinates[1] += act.getPickup_centroid_longitude();
						counter[0]++;
						counter[1]++;
					}
					averageCoordinates[0] = averageCoordinates[0]/counter[0];
					averageCoordinates[1] = averageCoordinates[1]/counter[1];
					while(!temp.isEmpty())
					{
						Servicio serTemp = temp.pop();
						double pKey = getDistance(serTemp.getPickup_centroid_latitude(), serTemp.getPickup_centroid_longitude(), averageCoordinates[0], averageCoordinates[1]);
						serTemp.setRelativePosition(pKey);
						int key = (int) Math.round(pKey);
						if (!coordenates.contains(key)){
							coordenates.put(key, new RedBlackBST<>());
						}
						if(!coordenates.get(key).contains(serTemp.getTaxi_id()))
						{
							coordenates.get(key).put(serTemp.getTaxi_id(), new DoubleLinkedList<>());
						}
						coordenates.get(key).get(serTemp.getTaxi_id()).add(serTemp);

					}
					String keyDelHash3c = act.getPickup_community_area()+"-"+act.getDropoff_community_area();
					if(arbol3C.contains(act.getFechaHoraInico())){
						if(arbol3C.get(act.getFechaHoraInico()).get(keyDelHash3c)!=null){
							arbol3C.get(act.getFechaHoraInico()).get(keyDelHash3c).add(act);
						}
						else{
							ArrayList<Servicio> arreglibiris = new ArrayList<>();
							arreglibiris.add(act);
							arbol3C.get(act.getFechaHoraInico()).put(keyDelHash3c,arreglibiris);
						}
					}
					else{
						LinearProbingHashST<String, ArrayList<Servicio>> nuevoHash = new LinearProbingHashST<>();
						ArrayList<Servicio> arregloDelHash = new ArrayList<>();
						arregloDelHash.add(act);
						nuevoHash.put(keyDelHash3c,arregloDelHash);
						arbol3C.put(act.getFechaHoraInico(),nuevoHash);
					}
				}
			}
		}


		 catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	
	@Override
	public IList<TaxiConServicios> A1TaxiConMasServiciosEnZonaParaCompania(int zonaInicio, String compania) {
		// TODO Auto-generated method stub
		IList<TaxiConServicios> resp =new DoubleLinkedList<>();
		BinaryHeapArray<Taxi> x = companias.get(compania);
		//System.out.println(x.size());
		int max = 0;
		for(int i = 0; i<x.size();i++) {
			Taxi a = x.get(i);

			BinaryHeapArray<Servicio> bh = a.getMapaA1().get(zonaInicio);
			if (bh == null) {

				continue;
			}
			else {


				int tam = bh.size();

			//		System.out.println("Tamanio hash : " +tam + "    "+i);


				TaxiConServicios anadir = new TaxiConServicios(a.getTaxi_id(), a.getCompany());
				if (tam == max) {
					resp.add(anadir);
					while(bh.size()!=0){
						//System.out.println("Ciclo 1");
						resp.get(resp.size()-1).getServicios().add(bh.get(0));
						bh.poll();
					}
				}
				else if (tam > max) {
					resp = new DoubleLinkedList<>();
					resp.add(anadir);
					max=tam;
					while(bh.size()!=0){
						//System.out.println("Ciclo 2");
						resp.get(resp.size()-1).getServicios().add(bh.get(0));
						if(i==6){

						//System.out.println("paso el agregar");
						}
						bh.poll();
						//System.out.println("Paso el remover");
					}
					if(i==6){
					//	System.out.println("salio de ahi");
					}
				}
			}
		}
		return resp;
	}


	@Override
	public IList<Servicio> A2ServiciosPorDuracion(int duracion) {
		// TODO Auto-generated method stub
		IList<Servicio> resp =new DoubleLinkedList<>();
		int mod60 = duracion%60;
		int dividido60 = mod60==0? duracion/60:(duracion/60)+1;
		ArrayList<Servicio> respuestaAntes=hashA2.get(dividido60);
		if(respuestaAntes==null){
			return resp;
		}
		else{
			for(Servicio s: respuestaAntes){
			resp.add(s);
			}
		}
		//System.out.println(resp.size()+ "Final resp");
		//System.out.println(respuestaAntes.size()+"RespuestaAntes");
		return resp;
	}


	@Override
	public IList<Servicio> B1ServiciosPorDistancia(double distanciaMinima, double distanciaMaxima) {
		// TODO Auto-generated method stub
		IList<Servicio> resp =new DoubleLinkedList<>();
		Iterable <Double> keys = arbolB1.keys(distanciaMinima, distanciaMaxima);

		for (Double d : keys)
		{

			if (d >= distanciaMinima && d <= distanciaMaxima)
			{
				ArrayList<Servicio> serviciosDeD = arbolB1.get(d);
				for (int i = 0; i<serviciosDeD.size(); i++) {
					resp.add(serviciosDeD.get(i));
				}
			}
			if(d>distanciaMaxima){
				break;
			}

		}
		return resp;
	}


	@Override
	public IList<Servicio> B2ServiciosPorZonaRecogidaYLlegada(int zonaInicio, int zonaFinal, String fechaI, String fechaF, String horaI, String horaF) {
		// TODO Auto-generated method stub
		IList<Servicio> resp = new DoubleLinkedList<>();
		String key = zonaInicio + "-" + zonaFinal;
		RedBlackBST<Date, ArrayList> tree = mapaB2.get(key);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		String dateformIn = fechaI + "T" + horaF;
		String dateformFin = fechaF + "T" + horaF;
		Date fechaini = null;
		Date fechaFini = null;
		try {
			fechaFini = sdf.parse(dateformFin);
			fechaini = sdf.parse(dateformIn);
		} catch (Exception e) {
			e.printStackTrace();
		}

		Iterable<Date> keys = tree.keys(fechaini, fechaFini);
		for (Date da : keys) {
			if (da.after(fechaini) && da.before(fechaFini)) {
				ArrayList<Servicio> serviciosDeD = tree.get(da);
				for (int i = 0; i < serviciosDeD.size(); i++) {
					resp.add(serviciosDeD.get(i));
				}
			}

		}
		return resp;
	}


	@Override
	public TaxiConPuntos[] R1C_OrdenarTaxisPorPuntos() {
		// TODO Auto-generated method stub
		TaxiConPuntos[] resp = new TaxiConPuntos[heapC1.size()];
		int i = 0;
		while(heapC1.size()!=0){
			resp[i] = heapC1.poll();
			i++;
		}
		return resp;
	}

	@Override
	public IList<Servicio> R2C_LocalizacionesGeograficas(String taxiIDReq2C, double millasReq2C, double latitudReq2C, double longitudReq2C) {
		// TODO Auto-generated method stub

			// TODO Auto-generated method stub
			DoubleLinkedList<Servicio> answer = new DoubleLinkedList<>();
			try {
				answer = coordenates.get((int) Math.round(millasReq2C)).get(taxiIDReq2C);
			}
			catch (NullPointerException p)
			{
				System.err.println("no servicios");
			}
			return answer;
	}

	@Override
	public IList<Servicio> R3C_ServiciosEn15Minutos(String fecha, String hora) {
		// TODO Auto-generated method stub
		String[] partimosLaHora = hora.split(":");
		int minutos = Integer.parseInt(partimosLaHora[1]);
		int dividido15 = minutos/15;
		int minutosMenor = 15*dividido15;
		int minutosMayor = minutosMenor+15;
		String nuevaHoraMenor = partimosLaHora[0]+":"+minutosMenor+":"+partimosLaHora[2];
		String nuevaHoraMayor = partimosLaHora[0]+":"+minutosMayor+":"+partimosLaHora[2];
		Iterable<Date> fechas = arbol3C.keys();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		IList<Servicio> resp = new DoubleLinkedList<>();
		try{
			String fechaInicial= fecha+"T"+nuevaHoraMenor;
			String fechaFinal = fecha+"T"+nuevaHoraMayor;
			Date fi =sdf.parse(fechaInicial);
			Date ff = sdf.parse(fechaFinal);
		for(Date f:fechas){
			if(f.after(fi)&&f.before(ff)){
				Iterable<String> llaves =arbol3C.get(f).keys();
				for(String s: llaves){
					String[] partida = s.split("-");
					if(!(partida[0].equalsIgnoreCase(partida[2]))){
						for(Servicio serv :arbol3C.get(f).get(s)){
							resp.add(serv);
						}
					}
				}
			}
			else if(f.after(ff)){
				break;
			}
		}
		}
		catch(Exception e){

		}
		return resp;
	}
	public double toRad(double x){
		return (x*Math.PI)/180;
	}

	public double getDistance (double lat1, double lon1, double lat2, double lon2)
	{

		// TODO Auto-generated method stub
		final int R = 6371*1000; // Radious of the earth in meters
		Double latDistance = toRad(lat2-lat1);
		Double lonDistance = toRad(lon2-lon1);
		Double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(toRad(lat1))
				* Math.cos(toRad(lat2)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2);
		Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		Double distance = R * c;
		return distance;
	}


}
